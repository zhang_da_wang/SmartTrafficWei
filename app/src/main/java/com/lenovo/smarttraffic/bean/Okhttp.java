package com.lenovo.smarttraffic.bean;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class Okhttp {
    public static void Okhttpclass(String canshu, String str, Callback callback){
        RequestBody requestBody= FormBody.create(MediaType.parse("text/html;charset=utf-8"),canshu);

        OkHttpClient okHttpClient=new OkHttpClient();
        Request request=new Request.Builder().url(str).post(requestBody).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(callback);
    }
}
