package com.lenovo.smarttraffic.bean;

public class SenseBean {
    private String name;
    private int value;
    private int imgs;

    public SenseBean(String name, int value, int imgs) {
        this.name = name;
        this.value = value;
        this.imgs = imgs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getImgs() {
        return imgs;
    }

    public void setImgs(int imgs) {
        this.imgs = imgs;
    }


}
