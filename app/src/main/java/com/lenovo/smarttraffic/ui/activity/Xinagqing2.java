package com.lenovo.smarttraffic.ui.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.BaseActivity;

public class Xinagqing2 extends BaseActivity {

    private TextView tvtype2;
    private TextView tvtime2;
    private TextView tvneirong2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tvtype2=findViewById(R.id.tvType2);
        tvtime2=findViewById(R.id.tvtime2);
        tvneirong2=findViewById(R.id.tvneirong2);
        Bundle b=getIntent().getExtras();
//        Log.e("aaaaaaaa",b.toString());
//        tvtime2.setText(b.getString("time"));
        tvneirong2.setText(b.getString("content"));
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_xinagqing2;
    }
}
