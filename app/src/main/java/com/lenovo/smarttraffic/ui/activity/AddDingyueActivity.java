package com.lenovo.smarttraffic.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.lenovo.smarttraffic.bean.MainData;
import com.lenovo.smarttraffic.R;

import java.util.ArrayList;

import butterknife.BindView;

public class AddDingyueActivity extends AppCompatActivity {

//    String[] dingyue1={"推荐","热点","科技","汽车资讯"};
//    String[] dingyue2={"健康","财经","教育","旅游","军事","实时路况","二手车","违章资讯","娱乐","体育"};
    ArrayList<String> my1 = new ArrayList<>();
    private GridView gv_top;
    private GridView gv_bottom;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    MyAdapter myAdapter;
    MyAdaptertwo myAdaptertwo;
//    private List<Item1Activity.TitleBean> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dingyue);

//        initToolBar(findViewById(R.id.toolbar), true, "订阅");
//        tvTitle.setText("订阅");

//        list= getIntent().getParcelableArrayListExtra("titleBean");

        my1.add("健康");
        my1.add("财经");
        my1.add("教育");
        my1.add("旅游");
        my1.add("军事");
        my1.add("实时路况");
        my1.add("文化");
        my1.add("二手车");
        my1.add("违章咨询");
        my1.add("娱乐");
        my1.add("体育");
        my1.add("视频");
        my1.add("游戏");
        my1.add("电影");

        gv_top=findViewById(R.id.gv_top);
        myAdapter=new MyAdapter();
        gv_top.setAdapter(myAdapter);

        gv_top.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                my1.add(MainData.titleList.get(i));

                MainData.titleList.remove(i);
                myAdapter.notifyDataSetChanged();
                myAdaptertwo.notifyDataSetChanged();
            }
        });

        gv_bottom=findViewById(R.id.gv_bottom);
        myAdaptertwo=new MyAdaptertwo();
        gv_bottom.setAdapter(myAdaptertwo);

        gv_bottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                MainData.titleList.add(my1.get(i));

                my1.remove(i);
                myAdapter.notifyDataSetChanged();
                myAdaptertwo.notifyDataSetChanged();
            }
        });
    }

    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return MainData.titleList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root=getLayoutInflater().inflate(R.layout.gird_dingyue,null);
            TextView tv=root.findViewById(R.id.tvdingyue);
            tv.setText(MainData.titleList.get(i));
            return root;
        }
    }

    class MyAdaptertwo extends BaseAdapter{

        @Override
        public int getCount() {
            return my1.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root=getLayoutInflater().inflate(R.layout.gird_dingyue,null);
            TextView tv=root.findViewById(R.id.tvdingyue);
            tv.setText(my1.get(i));
            return root;
        }
    }
}
