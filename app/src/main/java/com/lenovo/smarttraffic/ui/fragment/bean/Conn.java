package com.lenovo.smarttraffic.ui.fragment.bean;

public class Conn {
    public String content;
    private String imageUrl;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    private String time;
    private String titles;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Conn(String content, String imageUrl,String time,String titles) {
        this.time=time;
        this.titles=titles;
        this.content = content;
        this.imageUrl = imageUrl;
    }

    public Conn(String content,String time,String titles) {
        this.time=time;
        this.titles=titles;
        this.content = content;
    }

}
