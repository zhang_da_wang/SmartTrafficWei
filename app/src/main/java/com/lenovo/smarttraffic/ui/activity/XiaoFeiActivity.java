package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.lenovo.smarttraffic.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class XiaoFeiActivity extends AppCompatActivity {

    @BindView(R.id.btnDian)
    Button btnDian;
    @BindView(R.id.btnTing)
    Button btnTing;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.linearlayout)
    LinearLayout linearlayout;
    @BindView(R.id.listview_map)
    ListView listview;
    private MapView mapView;
    private AMap aMap;
    private MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
                                            //screen_orirntation_landscape;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xiao_fei);

        ButterKnife.bind(this);
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        myAdapter=new MyAdapter();
        listview.setAdapter(myAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(XiaoFeiActivity.this,MapTingChe.class));
            }
        });
    }

    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root=getLayoutInflater().inflate(R.layout.item_map,null);
            TextView tvNum=root.findViewById(R.id.tvNum);
            tvNum.setText(""+(i+1));
            return root;
        }
    }

    private void addMarker(){
        aMap.addMarker(new MarkerOptions()
        .position(new LatLng(39.941853,116.385307))
        .title("什刹海")
        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

    }

    @OnClick({R.id.btnDian,R.id.btnTing})
    public void onClick(View view){
        switch (view.getId())
        {
            case R.id.btnDian:
                addMarker();
                CameraPosition cameraPosition=new CameraPosition(new LatLng(39.941853,116.385307),15,0,0);
                CameraUpdate cameraUpdate= CameraUpdateFactory.newCameraPosition(cameraPosition);
                aMap.moveCamera(cameraUpdate);
                break;
            case R.id.btnTing:
                ll.setVisibility(view.VISIBLE);
                break;
            case R.id.linearlayout:
                ll.setVisibility(view.GONE);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /**
     * 根据动画按钮状态，调用函数animateCamera或moveCamera来改变可视区域
     */
    private void changeCamera(CameraUpdate update) {

        aMap.moveCamera(update);

    }
}
