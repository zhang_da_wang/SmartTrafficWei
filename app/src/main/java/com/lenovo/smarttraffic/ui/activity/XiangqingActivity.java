package com.lenovo.smarttraffic.ui.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.BaseActivity;

public class XiangqingActivity extends BaseActivity {

    private TextView tvTitle;
    private TextView tvtype;
    private TextView tvtime;
    private TextView tvneirong;
    private ImageView imgzixun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initToolBar(findViewById(R.id.toolbar), true, "详情");
//        //setContentView(R.layout.activity_xiangqing);
//        tvTitle = findViewById(R.id.tvTitle);
//        tvTitle.setText("详情");
        tvtype=findViewById(R.id.tvType);
        tvtime=findViewById(R.id.tvtime);
        tvneirong=findViewById(R.id.tvneirong);
        imgzixun=findViewById(R.id.imgzixun);
        Bundle b=getIntent().getExtras();
        tvtime.setText(b.getString("time"));
        tvneirong.setText(b.getString("content"));
        imgzixun.setImageURI(Uri.parse(b.getString("imgurl")));

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_xiangqing;
    }
}
