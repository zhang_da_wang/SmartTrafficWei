package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.MainActivity;
import com.lenovo.smarttraffic.bean.MainDate;
import com.lenovo.smarttraffic.R;

public class ShezhiActivity extends BaseActivity {

    private TextView title,tvqiandao,etc;
    private Button btnTui;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_shezhi);
        initToolBar(findViewById(R.id.toolbar), true, "设置");
        title=findViewById(R.id.tvTitle);
        title.setText("设置");
        btnTui=findViewById(R.id.tui);
        tvqiandao=findViewById(R.id.qiandaoqiandao);
        tvqiandao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShezhiActivity.this,QianDaoActivity.class));
            }
        });
        etc=findViewById(R.id.etc);
        etc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ShezhiActivity.this,ETCActivity.class));
            }
        });
        if (MainDate.isName==false){
            btnTui.setBackgroundColor(Color.parseColor("#D4D4D4"));
        }
        btnTui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainDate.isName==true){
                    startActivity(new Intent(ShezhiActivity.this, MainActivity.class));
                    MainDate.isName=false;
                }else {
                    Toast.makeText(ShezhiActivity.this, "还未登录", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_shezhi;
    }
}
