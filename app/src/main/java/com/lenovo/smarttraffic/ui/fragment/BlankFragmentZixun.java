package com.lenovo.smarttraffic.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.XiangqingActivity;
import com.lenovo.smarttraffic.ui.activity.Xinagqing2;
import com.lenovo.smarttraffic.ui.adapter.MyListAdapter;
import com.lenovo.smarttraffic.ui.fragment.bean.Conn;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragmentZixun extends Fragment {

    ArrayList<Conn> listData = new ArrayList<>();
    MyListAdapter myListAdapter;
    View contentView;

    public BlankFragmentZixun() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        Log.d("TAG","onStart");
        demo();
        super.onStart();
    }

    private void demo() {
        listData.add(new Conn("内容1","2019-02-11 10:33","标题1"));
        listData.add(new Conn("图片2","https://baike.baidu.com/pic/秦霄贤/23638239/1/ac6eddc451da81cb494dec9e5d66d0160924313d?fr=lemma&ct=single#aid=1&pic=ac6eddc451da81cb494dec9e5d66d0160924313d","2019-02-11 10:33","标题2"));
        listData.add(new Conn("内容3","2019-02-11 10:33","标题3"));
        listData.add(new Conn("内容4","2019-02-11 10:33","标题4"));
        listData.add(new Conn("图片5","https://baike.baidu.com/pic/秦霄贤/23638239/1/ac6eddc451da81cb494dec9e5d66d0160924313d?fr=lemma&ct=single#aid=1&pic=ac6eddc451da81cb494dec9e5d66d0160924313d","2019-02-11 10:33","标题5"));
        listData.add(new Conn("内容6","2019-2-09-08","标题6"));
        myListAdapter.setListData(listData);
        myListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d("TAG","onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //创建界面MsgFragment
        contentView = inflater.inflate(R.layout.fragment_blank_fragment_zixun, container, false);

        //界面的初始化
        myListAdapter = new MyListAdapter(getContext());
        ListView listView = (ListView) contentView.findViewById(R.id.listview);
        listView.setAdapter(myListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle bundle = new Bundle();
                if (listData.get(i).getImageUrl()==null){
                    bundle.putString("time", listData.get(i).getTime());
                    bundle.putString("content", listData.get(i).content);
                    Intent intent = new Intent(getContext(), Xinagqing2.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else {
                    bundle.putString("time", listData.get(i).getTime());
                    bundle.putString("content", listData.get(i).content);
                    bundle.putString("imgurl",listData.get(i).getImageUrl());
                    Intent intent = new Intent(getContext(),XiangqingActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
        return contentView;
    }
}
