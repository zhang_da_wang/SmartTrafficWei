package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.adapter.BasePagerAdapter;
import com.lenovo.smarttraffic.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.lenovo.smarttraffic.bean.MainData.titleList;

/**
 * @author Amoly
 * @date 2019/4/11.
 * description：
 */

public class Item1Activity extends BaseActivity {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tab_layout_list)
    TabLayout tabLayoutList;
    @BindView(R.id.header_layout)
    LinearLayout headerLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private ImageView img_xiangqing;
    private List<TitleBean> list;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitView();
        InitData();
    }
    @Override
    protected int getLayout() {
        return R.layout.activity_list_tab;
    }

    private void InitView() {
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item1));
        tvTitle.setText(getString(R.string.item1));
        tabLayoutList.setupWithViewPager(viewPager);
        tabLayoutList.setTabMode(TabLayout.MODE_SCROLLABLE);
        headerLayout.setBackgroundColor(CommonUtil.getInstance().getColor());
        img_xiangqing=findViewById(R.id.img_xiangqing);
        img_xiangqing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Item1Activity.this, AddDingyueActivity.class);
//                intent.putExtras((Intent) titleList);
//                intent.putParcelableArrayListExtra("titleBean",(ArrayList<TitleBean>) list);
                startActivity(intent);
            }
        });
    }

    private void InitData() {
        BasePagerAdapter basePagerAdapter = new BasePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(basePagerAdapter);
//        viewPager.setOffscreenPageLimit(2);
//
//
//        titleList=new ArrayList<>();
//        titleList.add("推荐");
//        titleList.add("热点");
//        titleList.add("科技");

        list=new ArrayList<>();
        list.add(new TitleBean("推荐",0));
        list.add(new TitleBean("热点",1));
        list.add(new TitleBean("科技",2));

        basePagerAdapter.setTitleList(titleList);
        basePagerAdapter.notifyDataSetChanged();

    }

    public static class TitleBean implements Parcelable{

        private String name;
        private int status;

        public TitleBean(String name, int status) {
            this.name = name;
            this.status = status;
        }

        protected TitleBean(Parcel in) {
            this.name = in.readString();
            this.status = in.readInt();
        }

        public final static Creator<TitleBean> CREATOR = new Creator<TitleBean>() {
            @Override
            public TitleBean createFromParcel(Parcel in) {
                return new TitleBean(in);
            }

            @Override
            public TitleBean[] newArray(int size) {
                return new TitleBean[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.name);
            parcel.writeInt(this.status);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        headerLayout.setBackgroundColor(CommonUtil.getInstance().getColor());
    }

}
