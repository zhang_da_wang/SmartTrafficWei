package com.lenovo.smarttraffic.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.BaseActivity;

public class UserActivity extends BaseActivity {
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_user);
        initToolBar(findViewById(R.id.toolbar), true, "用户中心");
        title=findViewById(R.id.tvTitle);
        title.setText("用户中心");


    }

    @Override
    protected int getLayout() {
        return R.layout.activity_user;
    }
}
