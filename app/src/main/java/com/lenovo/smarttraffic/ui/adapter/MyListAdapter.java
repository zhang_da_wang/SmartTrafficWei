package com.lenovo.smarttraffic.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.fragment.bean.Conn;

import java.util.ArrayList;

public class MyListAdapter extends BaseAdapter {

    private ArrayList<Conn> listData;
    private Context mContext;

    public ArrayList<Conn> getListData() {
        return listData;
    }

    public void setListData(
            ArrayList<Conn> listData) {
        this.listData = listData;
    }

    public MyListAdapter(Context context) {
        listData= new ArrayList<>();
        mContext=context;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (TextUtils.isEmpty(listData.get(i).getImageUrl())) {
            if (view == null) {
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list1,viewGroup,false);
            }
            Conn conn = (Conn) getItem(i);
            TextView texttitles = view.findViewById(R.id.newstitle);
            texttitles.setText(conn.getTitles());
            TextView texttime = view.findViewById(R.id.newstime);
            texttime.setText(conn.getTime());
            TextView textmessage = view.findViewById(R.id.newsmessage);
            textmessage.setText(conn.content);
            return view;
        } else {
            if (view == null) {
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list2, viewGroup, false);
            }
            Conn conn = (Conn) getItem(i);
            TextView textView = view.findViewById(R.id.newstitle_tu);
            textView.setText(conn.content);
            TextView texttime = view.findViewById(R.id.newstime);
            texttime.setText(conn.getTime());
            TextView textmessage = view.findViewById(R.id.newsmessage);
            textmessage.setText(conn.content);
            ImageView img=view.findViewById(R.id.img);
            String url=conn.getImageUrl();
            img.setImageURI(Uri.parse(url));
            return view;
        }
    }
}
