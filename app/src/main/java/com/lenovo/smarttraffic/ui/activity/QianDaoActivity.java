package com.lenovo.smarttraffic.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.BaseActivity;

public class QianDaoActivity extends BaseActivity {

    private TextView title;
    private WebView webView;
    private ProgressBar prowebview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_qian_dao);
        initToolBar(findViewById(R.id.toolbar), true, "用户签到");
        title=findViewById(R.id.tvTitle);
        title.setText("用户签到");

        prowebview=findViewById(R.id.prowebview);

        webView=findViewById(R.id.webview);

        webView.setWebChromeClient(webChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/myhtml.html");
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_qian_dao;
    }

    private WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress == 100) {
                prowebview.setVisibility(View.GONE);
            } else {
                if (prowebview.getVisibility() == View.GONE) {
                    prowebview.setVisibility(View.VISIBLE);
                }
                prowebview.setProgress(newProgress);

            }
            super.onProgressChanged(view, newProgress);
        }
    };
}
