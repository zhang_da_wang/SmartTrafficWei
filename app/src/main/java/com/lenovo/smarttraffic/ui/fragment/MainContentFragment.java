package com.lenovo.smarttraffic.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;
import com.lenovo.smarttraffic.bean.Bottom;
import com.lenovo.smarttraffic.bean.Constant;
import com.lenovo.smarttraffic.bean.MainDate;
import com.lenovo.smarttraffic.bean.Okhttp;
import com.lenovo.smarttraffic.ui.activity.QianDaoActivity;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.activity.SubWayActivity;
import com.lenovo.smarttraffic.ui.activity.UserActivity;
import com.lenovo.smarttraffic.bean.Weather;
import com.lenovo.smarttraffic.ui.activity.XiaoFeiActivity;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * @author Amoly
 * @date 2019/4/11.
 * description：主页面
 */
public class MainContentFragment extends BaseFragment {
    private static MainContentFragment instance = null;
    private PieChart pie;
    private Bottom bb;
    private int pm25;
    private int green;
    private Weather weather;
    private TextView today_temp,mor_temp,today_day,mor_day,ziwai,chuanyi,ganmao,yundong,ditie,yonghu,xiaofei,qiandao;
    private ImageView img1,img2;
    private ArrayList<Integer> allSenses = new ArrayList<>();

    public static MainContentFragment getInstance() {
        if (instance == null) {
            instance = new MainContentFragment();
        }
        return instance;
    }



    @Override
    protected View getSuccessView() {
        View view = View.inflate(getActivity(), R.layout.fragment_home, null);
        pie=view.findViewById(R.id.pie);
        today_temp=view.findViewById(R.id.today_temp);
        mor_temp=view.findViewById(R.id.mor_temp);
        today_day=view.findViewById(R.id.today_day);
        mor_day=view.findViewById(R.id.mor_day);

        ziwai=view.findViewById(R.id.ziwai);
        ganmao=view.findViewById(R.id.ganmao);
        yundong=view.findViewById(R.id.yundong);
        chuanyi=view.findViewById(R.id.chuanyi);

        ditie=view.findViewById(R.id.ditie);
        ditie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SubWayActivity.class));
            }

        });
        yonghu=view.findViewById(R.id.yonghu);
        yonghu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainDate.isName==true)
                {
                    startActivity(new Intent(getActivity(), UserActivity.class));
                }else {
                    Toast.makeText(getActivity(), "未登录", Toast.LENGTH_SHORT).show();
                }
            }

        });
        xiaofei=view.findViewById(R.id.xiaofei);
        xiaofei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), XiaoFeiActivity.class));
            }

        });
        qiandao=view.findViewById(R.id.qiandao);
        qiandao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), QianDaoActivity.class));
            }

        });

        img1=view.findViewById(R.id.img1);
        img2=view.findViewById(R.id.img2);


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getpie();

        setListener();
        return view;
    }

    public void date(){
        today_temp.setText(weather.getROWS_DETAIL().get(1).getTemperature());
        mor_temp.setText(weather.getROWS_DETAIL().get(2).getTemperature());
        today_day.setText(weather.getROWS_DETAIL().get(1).getType());
        mor_day.setText(weather.getROWS_DETAIL().get(2).getType());
        if (weather.getROWS_DETAIL().get(1).getType().equals("晴"))
        {
            Drawable drawable = getResources().getDrawable(R.drawable.qing);

            img1.setImageDrawable(drawable);
        }else if(weather.getROWS_DETAIL().get(1).getType().contains("雨")){
            Drawable drawable = getResources().getDrawable(R.drawable.yutian);

            img1.setImageDrawable(drawable);

        }else if(weather.getROWS_DETAIL().get(1).getType().equals("阴")){
            Drawable drawable = getResources().getDrawable(R.drawable.yintian);

            img1.setImageDrawable(drawable);
        }
        if (weather.getROWS_DETAIL().get(2).getType().equals("晴"))
        {
            Drawable drawable = getResources().getDrawable(R.drawable.qing);

            img2.setImageDrawable(drawable);
        }else if(weather.getROWS_DETAIL().get(2).getType().contains("雨")){
            Drawable drawable = getResources().getDrawable(R.drawable.yutian);

            img2.setImageDrawable(drawable);

        }else if(weather.getROWS_DETAIL().get(2).getType().equals("阴")){
            Drawable drawable = getResources().getDrawable(R.drawable.yintian);

            img2.setImageDrawable(drawable);
        }
    }

    private void setListener() {

    }

    @Override
    protected Object requestData() {
        return Constant.STATE_SUCCEED;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onDestroy() {
        if (instance != null) {
            instance = null;
        }
        super.onDestroy();
    }

    public void erdate(){
        allSenses.add(bb.getLightIntensity());
        allSenses.add(bb.getCo2());
        allSenses.add(bb.getCo2());
        allSenses.add(bb.getHumidity());

        if (allSenses.get(0) < 1000) {
            ziwai.setText("弱");
            ziwai.setTextColor(Color.parseColor("#4472c4"));
        } else if (allSenses.get(0) > 3000) {
            ziwai.setText("强");
            ziwai.setTextColor(Color.parseColor("#ff0000"));

        } else {
            ziwai.setText("中等");
            ziwai.setTextColor(Color.parseColor("#00b050"));
        }

        if (allSenses.get(1) > 0 && allSenses.get(1) < 300) {
            yundong.setText("适宜");
            yundong.setTextColor(Color.parseColor("#44dc68"));
        } else if (allSenses.get(1) >= 300 && allSenses.get(1) <= 6000) {
            yundong.setText("中");
            yundong.setTextColor(Color.parseColor("#ffc000"));
        } else {

            yundong.setText("较不宜");
            yundong.setTextColor(Color.parseColor("#8149ac"));
        }

        if (allSenses.get(2) < 12) {
            chuanyi.setText("冷");
            chuanyi.setTextColor(Color.parseColor("#3462f4"));

        } else if (allSenses.get(2) >= 12 && allSenses.get(2) <= 21) {
            chuanyi.setText("舒适");
            chuanyi.setTextColor(Color.parseColor("#92d050"));
        } else if (allSenses.get(2) > 21 && allSenses.get(2) < 35) {
            chuanyi.setText("温暖");
            chuanyi.setTextColor(Color.parseColor("#44dc68"));
        } else {
            chuanyi.setText("热");
            chuanyi.setTextColor(Color.parseColor("#ff0000"));
        }

        if (allSenses.get(3) < 50) {
            ganmao.setText("较易发");
            ganmao.setTextColor(Color.parseColor("#ff0000"));
        } else {
            ganmao.setText("少发");
            ganmao.setTextColor(Color.parseColor("#ffff40"));

        }
    }

    public void getpie(){

        String canshu="{\"UserName\":\"user1\"}";

        //String str="http://192.168.1.112:8088/transportservice/action/GetWeather.do";
        String str="http://24782126vm.zicp.vip:27856/transportservice/action/GetWeather.do";

        Okhttp.Okhttpclass(canshu,str,new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("输出", "失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                Gson gson = new Gson();

                weather = gson.fromJson(result, Weather.class);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Toast.makeText(getActivity().getApplicationContext(), ""+pm25+green, Toast.LENGTH_SHORT).show();
                        date();
                        erdate();
                    }
                });
            }
        });

        //String str2="http://192.168.1.112:8088/transportservice/action/GetAllSense.do";
        String str2="http://24782126vm.zicp.vip:27856/transportservice/action/GetAllSense.do";

        Okhttp.Okhttpclass(canshu,str2,new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("输出", "失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String result2 = response.body().string();
                Gson gson = new Gson();

                bb = gson.fromJson(result2, Bottom.class);


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Toast.makeText(getActivity().getApplicationContext(), ""+pm25+green, Toast.LENGTH_SHORT).show();
                        pm25=bb.get_$Pm2574();
                        green=1000-pm25;
                        pieclass();
                    }
                });

            }
        });
    }

    public void pieclass(){


        ArrayList<PieEntry> entries=new ArrayList<>();
        entries.add(new PieEntry(pm25,""));
        entries.add(new PieEntry(green,""));

        PieDataSet dataSet=new PieDataSet(entries,"");
        dataSet.setColors(Color.parseColor("#F1E52B"),Color.parseColor("#23F177"));
        dataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return "";
            }
        });
        PieData data=new PieData(dataSet);
        pie.setData(data);
        if (bb.get_$Pm2574() > 0 && bb.get_$Pm2574() < 35) {
            pie.setCenterText("优");
        } else if (bb.get_$Pm2574() >= 35 && bb.get_$Pm2574() <= 75) {
            pie.setCenterText("良");
        } else if (bb.get_$Pm2574() >= 75 && bb.get_$Pm2574() <= 115) {
            pie.setCenterText("轻度污染");
        }else if (bb.get_$Pm2574() >= 115 && bb.get_$Pm2574() <= 150) {
            pie.setCenterText("中度污染");
        }else if (bb.get_$Pm2574() >= 150 ) {
            pie.setCenterText("重度污染");
        }
        pie.setDescription(null);
        Legend legend=new Legend();
        legend=pie.getLegend();
        // 取消图例
        legend.setEnabled(false);
    }
}
