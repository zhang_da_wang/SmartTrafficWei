package com.lenovo.smarttraffic.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;

public class SettingActivity extends Activity {
    private TextView tvTitle;
    private EditText et1,et2,et3,et4;
    private Button btnSubmit;
    private EditText[] etnum= new EditText[4];
    private int ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//横屏

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        etnum[0]=findViewById(R.id.et1);
        etnum[1]=findViewById(R.id.et2);
        etnum[2]=findViewById(R.id.et3);
        etnum[3]=findViewById(R.id.et4);
        tvTitle=findViewById(R.id.tvTitle);
        tvTitle.setText("IP设置");



        btnSubmit=findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getEditText();
            }
        });

        for (int i=0;i<etnum.length;i++) {
            int g = i;
            etnum[g].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().length() > 1) {
                        ip = Integer.valueOf(charSequence.toString());
                        if (ip > 225) {
                            Toast.makeText(SettingActivity.this, "您输入的有误，请重新输入", Toast.LENGTH_SHORT).show();
                            etnum[g].getText().clear();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }
    }

    private void getEditText(){
        if (et1.getText().toString().length()<=0||et2.getText().toString().length()<=0||et3.getText().toString().length()<=0||et4.getText().toString().length()<=0){
            Toast.makeText(this, "不得为空", Toast.LENGTH_SHORT).show();
        }else {
                Toast.makeText(this, "提交成功", Toast.LENGTH_SHORT).show();
        }
    }
}
