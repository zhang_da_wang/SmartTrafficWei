package com.lenovo.smarttraffic.ui.activity;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.gson.Gson;
import com.lenovo.smarttraffic.bean.Bottom;
import com.lenovo.smarttraffic.bean.Okhttp;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.Weather;
import com.lenovo.smarttraffic.bean.SenseBean;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class WeatherActivity extends BaseActivity {

    private GridView gridView,gridView_bottom;
    private ImageView todayimg;
    private LineChart line;
    private TextView todaydate,todaytemp,todaywea;
    private Weather weather = new Weather();
    private ArrayList<Integer> BigTemp=new ArrayList();
    private ArrayList<Integer> SmarlTemp=new ArrayList();
    private MyAdapter myAdapter;
    private MyAdapterTwo adapterTwo;
    private Bottom bb;
    private ArrayList<Integer> allSenses = new ArrayList<>();
    private Handler mHandler=new Handler();
    private TextView title;
    private String[] ZhiShus = {"紫外线指数", "空气指数", "运动指数", "穿衣指数", "感冒指数", "洗车指数"};
    private int[] imgs = {R.mipmap.icon101, R.mipmap.icon105, R.mipmap.icon104, R.mipmap.icon103, R.mipmap.icon102, R.mipmap.icon105};


    private Handler mHeadler = new Handler();

    private List<SenseBean> senseList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        senseList=new ArrayList<>();
        //setContentView(R.layout.activity_weather);
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item3));
        intiView();
        getok();
        getok2();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        initAllSense();
//        todayimgview();

        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                getok2();
//                initAllSense(bb);

                MyAdapterTwo adapterTwo=new MyAdapterTwo();
                gridView_bottom.setAdapter(adapterTwo);
                adapterTwo.setListb(senseList);
                adapterTwo.notifyDataSetChanged();
                mHandler.postDelayed(this,3000);
            }
        };
        mHandler.postDelayed(runnable,3000);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_weather;
    }

    public void initAllSense(Bottom sense){
//        allSenses.clear();
//        allSenses.add(bb.getLightIntensity());
//        allSenses.add(bb.get_$Pm2574());
//        allSenses.add(bb.getCo2());
//        allSenses.add(bb.getTemperature());
//        allSenses.add(bb.getHumidity());
//        allSenses.add(0);
        senseList.clear();
        senseList.add(new SenseBean("紫外线指数",sense.getLightIntensity(),R.mipmap.icon101));
        senseList.add(new SenseBean("空气污染指数",sense.get_$Pm2574(),R.mipmap.icon105));
        senseList.add(new SenseBean("运动指数",sense.getCo2(),R.mipmap.icon104));
        senseList.add(new SenseBean("穿衣指数",sense.getTemperature(),R.mipmap.icon103));
        senseList.add(new SenseBean("感冒指数",sense.getHumidity(),R.mipmap.icon102));
        senseList.add(new SenseBean("洗车指数",0,R.mipmap.icon105));
        adapterTwo.setListb(senseList);
        adapterTwo.notifyDataSetChanged();
    }

    //日期转星期
    public String getWeek(String sdata){
        Date date=strToDate(sdata);
        Calendar c=Calendar.getInstance();
        c.setTime(date);
        int hour=c.get(Calendar.DAY_OF_WEEK);
        //hour中存的是星期几范围1-7
        //1代表星期日，7代表星期六
        return new SimpleDateFormat("EE").format(c.getTime());

    }

    public Date strToDate(String strDate){
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
        ParsePosition pos=new ParsePosition(0);
        Date strtodate=formatter.parse(strDate,pos);
        return strtodate;
    }

    public void linedate(){
        for (int i=0;i<6;i++)
        {
            String temp=weather.getROWS_DETAIL().get(i).getTemperature();
            SmarlTemp.add(Integer.parseInt(temp.substring(0, temp.indexOf("~"))));
            BigTemp.add(Integer.parseInt(temp.substring(temp.indexOf("~")+1,temp.length())));

        }
        Log.e("big", String.valueOf(BigTemp));
        Log.e("smarl", String.valueOf(SmarlTemp));
    }

    public void lineclass(){
        ArrayList<Entry> entriesBig=new ArrayList<>();
        ArrayList<Entry> entriesSmarl=new ArrayList<>();
        for (int i=0;i<weather.getROWS_DETAIL().size();i++)
        {
            entriesBig.add(new Entry(i,BigTemp.get(i)));
            entriesSmarl.add(new Entry(i,SmarlTemp.get(i)));
        }
        LineDataSet lineBigDataSet=new LineDataSet(entriesBig,"");
        lineBigDataSet.setColor(Color.parseColor("#F1D6A4"));
        lineBigDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineBigDataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return (int)value+"°";
            }
        });

        LineDataSet lineSmartDataSet=new LineDataSet(entriesSmarl,"");
        lineSmartDataSet.setColor(Color.parseColor("#5BE7F1"));
        lineSmartDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineSmartDataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return (int)value+"°";
            }
        });


        LineData data=new LineData(lineBigDataSet,lineSmartDataSet);
        line.setData(data);
        line.setDescription(null);
        Legend legend = new Legend();
        // 获取lineChart图例
        legend=line.getLegend();
        // 取消图例
        legend.setEnabled(false);
        XAxis xAxis=line.getXAxis();
        xAxis.setEnabled(false);

        YAxis yLeftAxis=line.getAxisLeft();
        yLeftAxis.setEnabled(false);


        YAxis yRightAxis=line.getAxisRight();
        yRightAxis.setEnabled(false);

    }

    public void intiView(){
        todaydate=findViewById(R.id.date);
        todaytemp=findViewById(R.id.temp);
        todaywea=findViewById(R.id.weath);
        line=findViewById(R.id.line);

        title=findViewById(R.id.tvTitle);
        title.setText("天气预报");


        todayimg=findViewById(R.id.todayimg);
        gridView=findViewById(R.id.gridone);
        myAdapter=new MyAdapter();
        gridView.setAdapter(myAdapter);

        gridView_bottom=findViewById(R.id.gridView_bottom);
        adapterTwo=new MyAdapterTwo();
        gridView_bottom.setAdapter(adapterTwo);
    }

    class MyAdapter extends BaseAdapter{
        private List<Weather.ROWSDETAILBean> list;
        public MyAdapter(){
            list=new ArrayList<>();
        }
        public List<Weather.ROWSDETAILBean> getList(){
            return list;
        }
        public void setList(List<Weather.ROWSDETAILBean> list){
            this.list=list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root= getLayoutInflater().inflate(R.layout.item,null);
            final TextView tvmon=root.findViewById(R.id.mon);
            final TextView tvweek=root.findViewById(R.id.week);
            final TextView tvwea=root.findViewById(R.id.wea);
            final ImageView imgone=root.findViewById(R.id.imgone);

            tvmon.setText(""+list.get(i).getWData());
            tvweek.setText(""+getWeek(list.get(i).getWData()));
            tvwea.setText(""+list.get(i).getType());
            if (list.get(i).getType().contains("晴"))
            {
                Drawable drawable = getResources().getDrawable(R.drawable.qing);

                imgone.setImageDrawable(drawable);
            }else if(list.get(i).getType().contains("雨")){
                Drawable drawable = getResources().getDrawable(R.drawable.yutian);

                imgone.setImageDrawable(drawable);

            }else if(list.get(i).getType().contains("阴")){
                Drawable drawable = getResources().getDrawable(R.drawable.yintian);

                imgone.setImageDrawable(drawable);
            }
            todayimgview();
            return root;
        }

        public void todayimgview(){
            final String temp=list.get(1).getTemperature();
            //final String wea=weather.getROWS_DETAIL().get(1).getType();
            final String weaimg=list.get(1).getType();
            if (weaimg.equals("晴"))
            {
                Drawable drawable = getResources().getDrawable(R.drawable.qing);

                todayimg.setImageDrawable(drawable);
            }else if(weaimg.contains("雨")){
                Drawable drawable = getResources().getDrawable(R.drawable.yutian);

                todayimg.setImageDrawable(drawable);

            }else if(weaimg.equals("阴")){
                Drawable drawable = getResources().getDrawable(R.drawable.yintian);

                todayimg.setImageDrawable(drawable);
            }
            todaydate.setText(list.get(1).getWData());
            todaytemp.setText(temp.substring(0, temp.indexOf("~")));
            todaywea.setText(weaimg.toString());
        }
    }

    class MyAdapterTwo extends BaseAdapter{

        private List<SenseBean> listb;
        public MyAdapterTwo(){
            listb=new ArrayList<>();
        }

        public List<SenseBean> getListb() {
            return listb;
        }

        public void setListb(List<SenseBean> listb) {
            this.listb = listb;
        }
        @Override
        public int getCount() {
            return listb.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root = getLayoutInflater().inflate(R.layout.item_bottom, null);
            final TextView tvzhishu = root.findViewById(R.id.zhishu);
            final ImageView imgtwo = root.findViewById(R.id.imgtwo);
            final TextView tvnum = root.findViewById(R.id.tvnum);
            final TextView tvqing = root.findViewById(R.id.qiang);

            tvnum.setText(listb.get(i).getName());
            imgtwo.setImageResource(listb.get(i).getImgs());
            tvzhishu.setText(String.valueOf(listb.get(i).getValue()));

            if (listb.get(i).getName().equals("紫外线指数")) {
                if (listb.get(i).getValue()> 0 && listb.get(i).getValue() < 1000) {
                    tvqing.setText("弱");
                    tvqing.setTextColor(Color.parseColor("#4472c4"));
                } else if (listb.get(i).getValue() >= 1000 && listb.get(i).getValue() <= 3000) {
                    tvqing.setText("中等");
                    tvqing.setTextColor(Color.parseColor("#00b050"));
                } else if (listb.get(i).getValue() > 3000) {
                    tvqing.setText("强");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                }
            }
            if (listb.get(i).getName().equals("空气污染指数")) {
                if (listb.get(i).getValue() > 0 && listb.get(i).getValue() < 35) {
                    tvqing.setText("优");
                    tvqing.setTextColor(Color.parseColor("#44dc68"));
                } else if (listb.get(i).getValue() >= 35 && listb.get(i).getValue() <= 75) {
                    tvqing.setText("良");
                    tvqing.setTextColor(Color.parseColor("#92d050"));
                } else if (listb.get(i).getValue() >= 75 && listb.get(i).getValue() <= 115) {
                    tvqing.setText("轻度污染");
                    tvqing.setTextColor(Color.parseColor("#ffff40"));
                }else if (listb.get(i).getValue() >= 115 && listb.get(i).getValue() <= 150) {
                    tvqing.setText("中度污染");
                    tvqing.setTextColor(Color.parseColor("#bf9000"));
                }else if (listb.get(i).getValue() >= 150 ) {
                    tvqing.setText("重度污染");
                    tvqing.setTextColor(Color.parseColor("#993300"));
                }
            }
            if (listb.get(i).getName().equals("运动指数")) {
                if (listb.get(i).getValue() > 0 && listb.get(i).getValue() < 3000) {
                    tvqing.setText("适宜");
                    tvqing.setTextColor(Color.parseColor("#44dc68"));
                } else if (listb.get(i).getValue() >= 3000 && listb.get(i).getValue() <= 6000) {
                    tvqing.setText("中");
                    tvqing.setTextColor(Color.parseColor("#ffc000"));
                }else if(listb.get(i).getValue() >6000){
                    {
                        tvqing.setText("较不宜");
                        tvqing.setTextColor(Color.parseColor("#8149ac"));
                    }
                }
            }
            if (listb.get(i).getName().equals("穿衣指数")) {
                if (listb.get(i).getValue() < 12) {
                    tvqing.setText("冷");
                    tvqing.setTextColor(Color.parseColor("#3462f4"));
                } else if (listb.get(i).getValue() >= 12 && listb.get(i).getValue() <= 21) {
                    tvqing.setText("舒适");
                    tvqing.setTextColor(Color.parseColor("#92d050"));
                } else if (listb.get(i).getValue() > 21&& listb.get(i).getValue() <= 35) {
                    tvqing.setText("温暖");
                    tvqing.setTextColor(Color.parseColor("#44dc68"));
                }else if(listb.get(i).getValue() > 35){
                    tvqing.setText("热");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                }
            }
            if (listb.get(i).getName().equals("感冒指数")) {
                if (listb.get(i).getValue() < 8) {
                    tvqing.setText("较易发");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                } else if (listb.get(i).getValue() >= 8) {
                    tvqing.setText("少发");
                    tvqing.setTextColor(Color.parseColor("#ffff40"));
                }
            }
            if (listb.get(i).getName().equals("洗车指数")) {
                if (listb.get(i).getValue() < 8) {
                    tvqing.setText("不适宜");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                } else if (listb.get(i).getValue() >= 8) {
                    tvqing.setText("不太适宜");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                } else if (listb.get(i).getValue() >= 8) {
                    tvqing.setText("适宜");
                    tvqing.setTextColor(Color.parseColor("#ff0000"));
                }
            }
            return root;
        }


    }

    public void getok(){
        String canshu="{\"UserName\":\"user1\"}";
        //String str="http://192.168.1.112:8088/transportservice/action/GetWeather.do";
        String str="http://24782126vm.zicp.vip:27856/transportservice/action/GetWeather.do";

        Okhttp.Okhttpclass(canshu,str,new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("输出", "失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                Gson gson = new Gson();

                weather = gson.fromJson(result, Weather.class);
                linedate();

                lineclass();
                myAdapter.setList(weather.getROWS_DETAIL());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        linedate();

                        lineclass();
                        myAdapter.notifyDataSetChanged();

                        Toast.makeText(WeatherActivity.this, "网络连接成功" , Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    private void getok2() {
        String canshu="{\"UserName\":\"user1\"}";
        //String str2="http://192.168.1.112:8088/transportservice/action/GetAllSense.do";
        String str2="http://24782126vm.zicp.vip:27856/transportservice/action/GetAllSense.do";

        Okhttp.Okhttpclass(canshu,str2,new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("输出", "失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.code()==200){
                    String result2 = response.body().string();

                    Gson gson = new Gson();

                    bb = gson.fromJson(result2, Bottom.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initAllSense(bb);
//                        Toast.makeText(WeatherActivity.this, "网络连接成功"+weather.getROWS_DETAIL().get(1).getWData(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }


}
