package com.lenovo.smarttraffic.ui.activity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.bean.MainDate;
import com.lenovo.smarttraffic.bean.Okhttp;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.db.DBHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ETCActivity extends BaseActivity {

    private TextView tvtitle,tvyue,tvchacheng,tvchongcheng;
    private Spinner sp1,sp2,sp3;
    private int spchecked1,spchecked2,spchecked3;
    private Button btncha,btnchong,btncha2;
    private ListView lv;
    private Cursor cursor;
    private SQLiteDatabase db;
    private ArrayList<Integer> xuhao=new ArrayList();
    private ArrayList<Integer> chehao=new ArrayList();
    private ArrayList<Integer> jine=new ArrayList();
    private ArrayList<String> chonguser=new ArrayList();
    private ArrayList<String> chongtime=new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etc);

        initToolBar(findViewById(R.id.toolbar), true, "ETC账户");
        tvtitle=findViewById(R.id.tvTitle);
        tvtitle.setText("ETC账户");

        tvyue=findViewById(R.id.yue);
        tvchacheng=findViewById(R.id.chacheng);
        tvchongcheng=findViewById(R.id.chongcheng);
        sp1=findViewById(R.id.sp1);
        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //spchecked1= ETCActivity.this.getResources().getIntArray((R.array.sp1))[i];
                spchecked1= Integer.parseInt((String) sp1.getItemAtPosition(i));
                Toast.makeText(ETCActivity.this, ""+spchecked1, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp2=findViewById(R.id.sp2);
        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               // spchecked2= ETCActivity.this.getResources().getIntArray((R.array.sp2))[i];
                spchecked2= Integer.parseInt((String) sp2.getItemAtPosition(i));
                Toast.makeText(ETCActivity.this, ""+spchecked2, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp3=findViewById(R.id.sp3);
        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //spchecked3= ETCActivity.this.getResources().getIntArray((R.array.sp1))[i];
                spchecked3= Integer.parseInt((String) sp1.getItemAtPosition(i));
                Toast.makeText(ETCActivity.this, ""+spchecked3, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btncha=findViewById(R.id.cha);
        btncha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getyue();
            }
        });

        btnchong=findViewById(R.id.chong);
        btnchong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setchong();
            }
        });

        lv=findViewById(R.id.lv);

        btncha2=findViewById(R.id.cha2);
        btncha2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getsql();
                MyAdapter myAdapter=new MyAdapter();
                lv.setAdapter(myAdapter);
                xuhao.clear();
                chehao.clear();
                jine.clear();
                chonguser.clear();
                chongtime.clear();
            }
        });

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_etc;
    }

    public void getyue(){
        String canshu="{\"CarId\":"+spchecked1+", \"UserName\":\"user1\"} ";
        //String str="http://192.168.1.112:8088/transportservice/action/GetCarAccountBalance.do ";
        String str="http://24782126vm.zicp.vip:27856/transportservice/action/GetCarAccountBalance.do";

        Okhttp.Okhttpclass(canshu, str, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("错误","网络错误");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result=response.body().string();

                try {
                    JSONObject jo=new JSONObject(result);
                    final String Balance = jo.getString("Balance");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvyue.setText(Balance+"元");
                            tvchacheng.setText("查询成功");
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });
    }

    public void setchong(){
        String canshu="{\"CarId\":"+spchecked1+",\"Money\":"+spchecked2+", \"UserName\":\"user1\"}";
        //String str="http://192.168.1.112:8088/transportservice/action/SetCarAccountRecharge.do  ";
        String str="http://24782126vm.zicp.vip:27856/transportservice/action/SetCarAccountRecharge.do";

        Okhttp.Okhttpclass(canshu, str, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("错误","网络错误");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result=response.body().string();

                try {
                    JSONObject jo=new JSONObject(result);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           tvchongcheng.setText("充值成功");
                            getyue();
                            setsql();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setsql(){

        DBHelper dbHelper=new DBHelper(ETCActivity.this);
        SQLiteDatabase db=dbHelper.getWritableDatabase();


        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");

        ContentValues values=new ContentValues();
        values.put("CardId",spchecked1);
        values.put("Money",spchecked2);
        values.put("User", MainDate.username);
        values.put("Time",simpleDateFormat.format(date));

        db.insert("user",null,values);
        db.close();
    }

    public void getsql(){
        DBHelper dbHelper=new DBHelper(ETCActivity.this);
        db=dbHelper.getReadableDatabase();
        cursor=db.query("user",null,null,null,null,null,null);
        while (cursor.moveToNext()){
            xuhao.add(cursor.getInt(cursor.getColumnIndex("_id")));
            chehao.add(cursor.getInt(cursor.getColumnIndex("CardId")));
            jine.add(cursor.getInt(cursor.getColumnIndex("Money")));
            chonguser.add(cursor.getString(cursor.getColumnIndex("User")));
            chongtime.add(cursor.getString(cursor.getColumnIndex("Time")));
        }
    }


    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return xuhao.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View root=getLayoutInflater().inflate(R.layout.item_jilu,null);
            final TextView tvxuhao=root.findViewById(R.id.xuhao);
            final TextView tvchehao=root.findViewById(R.id.chehao);
            final TextView tvjine=root.findViewById(R.id.jine);
            final TextView tvchonguser=root.findViewById(R.id.chonguser);
            final TextView tvchongtime=root.findViewById(R.id.chongtime);

            tvxuhao.setText(""+xuhao.get(i));
            tvchehao.setText(""+chehao.get(i));
            tvjine.setText(""+jine.get(i));
            tvchonguser.setText(chonguser.get(i));
            tvchongtime.setText(chongtime.get(i));

            return root;
        }
    }
}
